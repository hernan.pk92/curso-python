# -*- coding: utf-8 -*-

def foreign_exchange_calculator(ammount):
    usd_to_cop_rate = 3439.00

    return usd_to_cop_rate * ammount

def run():
    print('C A L C U L A D O R A  D E  D I V I S A')
    print('Convierte dolar a pesos colombianos.')
    print('')

    ammount = float(raw_input('Ingresa la cantidad de dolares que quieres convertir: '))

    result = foreign_exchange_calculator(ammount)

    print('${} dolar son ${} pesos colombianos'.format(ammount, result))

if __name__ == '__main__':
    run()
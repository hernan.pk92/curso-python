# -*- coding: utf-8 -*-

def fact(num):
    if num == 0:
        return 1
    else:
        return num * fact(num-1)

def run():
    num = int(raw_input('Ingrese numero para sacar factorial: '))
    factorial = fact(num)
    print('El factorial de {}! es: {}'.format(num,factorial))

if __name__ == "__main__":
    run()
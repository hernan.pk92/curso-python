"""
"abacabad" c
"abacabaabacaba" _
"abcdefghijklmnopqrstuvwxyziflskecznslkjfabe" d
"bcccccccccccccyb" y
"""

def first_not_repeating_char(char_sequence):
    
    char_sequence_tupla = tuple(char_sequence)

    for idx in range(len(char_sequence)):
        letter = char_sequence_tupla[idx]
        count = 0

        for letter_repeat in range(len(char_sequence)):
            if char_sequence_tupla[letter_repeat] == letter:
                count += 1

        if count >= 2:
            continue
        else:
            return letter

    return '_'

if __name__ == '__main__':
    char_sequence = str(raw_input('Escribe una secuencia de caracteres: '))

    result = first_not_repeating_char(char_sequence)

    if result == '_':
        print('Todos los caracteres se repiten.')
    else:
        print('El primer caracter no repetido es: {}'.format(result))
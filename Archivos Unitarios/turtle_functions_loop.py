# -*- coding: utf-8 -*-

import turtle

def main():
    turtle.Screen()
    dave = turtle.Turtle()

    make_square(dave)

    turtle.mainloop()

def make_square(dave):
    for i in range(500):
        make_line_and_turn(dave, i)

def make_line_and_turn(dave, length):
    dave.forward(length)
    dave.left(90)

if __name__ == '__main__':
    main()
# -*- coding: utf-8 -*-

from Lamp import Lamp

def run():
    lamp = Lamp(is_turned_on= True)

    while True:
        command = str(raw_input('''
            ¿Que deseas hacer?

            [p] Prender
            [a] Apagar
            [s] Salir
        '''))

        if command == 'p':
            lamp.turn_on()
        elif command == 'a':
            lamp.turn_off()
        else:
            break

if __name__ == "__main__":
    run()